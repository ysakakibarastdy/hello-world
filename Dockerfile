FROM caddy:2.4.3

COPY ./srv /srv
COPY ./Caddyfile /etc/caddy/Caddyfile

WORKDIR /
ENTRYPOINT ["/usr/bin/caddy", "run", "--config", "/etc/caddy/Caddyfile"]